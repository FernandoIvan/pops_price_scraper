require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'pry'
require 'json'

def write_to_file(pops)
  price_list = File.open('./pops_price_list.json', 'w')
  price_list.write(pops.to_json)
  price_list.close
end

def start_scrape
  pops_price_file = File.open("./pops_price_list.json").read
  if pops_price_file.empty?
    pops_price_list = {}
  else
    pops_price_list = JSON.parse(pops_price_file)
  end

  market_index_page = 'https://www.hobbydb.com/marketplaces/poppriceguide/collectibles/for_sale_search?'
  page = Nokogiri::HTML(open(market_index_page))

  # number of pages we will iterate through
  num_of_pages = page.css('.page').last.children.text.strip.to_i

  # list of pops
  collectable_list = page.css('.collectible-info')
  collectable_list.each do |collectable|
    # collectable returns column with information
    pop = {}
    price = collectable.css('.collectible-price').text.strip

    # get third party price if avaliable
    third_party_price = collectable
                        .css('.collectible-for-sale-from')
                        .children&.children&.last&.text&.strip

    pop_name = collectable.css('.collectible-name').text.strip
      if !pops_price_list.key?(pop_name)
        pops_price_list[pop_name] = [
          {
          date: Time.now.to_s,
          price: price,
          third_party_price: third_party_price,
          }
        ]
      else
        pops_price_list[pop_name] << {
          date: Time.now.to_s,
          price: price,
          third_party_price: third_party_price
        }
      end
    # TODO: Make a shipping price with regex to match as a float
    # pop.shipping_price = collectable.css("")
  end

  write_to_file(pops_price_list)
end

start_scrape